package de.friction.landingdistance_a306;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


public class InputActivity extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemSelectedListener
{
    private static final int REFERENCE_WEIGHT = 300;

    // Index 0 = flapSetting, index 1 = Braking Mode

    // Reference Distances for DRY 30/40, 15/20
    int[][][] referenceDistance = {
            {{3600, 4710, 7070}, {4190, 5420, 8230}},   // DRY
            {{4570, 4820, 7070}, {5560, 5750, 8230}},   // GOOD
            {{5430, 5470, 7070}, {6370, 6420, 8230}},   // GOOD-MEDIUM
            {{6130, 6170, 7210}, {7240, 7270, 8430}},   // MEDIUM
            {{6930, 6950, 7550}, {8400, 8410, 9060}},   // MEDIUM-POOR
            {{8290, 8290, 8620}, {9460, 9460, 9870}}};  // POOR

    // Weight adjustment per 10.000 lbs if actual weight is below reference weight of 300.000 lbs
    int[][][] penaltyWeightBelow = {
            {{-70, -90, -140}, {-100, -140, -220}},     // DRY
            {{-100, -100, -140}, {-170, -170, -220}},   // GOOD
            {{-110, -110, -140}, {-160, -160, -220}},   // GOOD-MEDIUM
            {{-120, -120, -140}, {-190, -190, -220}},   // MEDIUM
            {{-150, -150, -150}, {-240, -240, -240}},   // MEDIUM-POOR
            {{-180, -180, -190}, {-280, -280, -290}}};  // POOR

    // Weight adjustment per 10.000 lbs if actual weight is above reference weight of 300.000 lbs
    int[][][] penaltyWeightAbove = {
            {{280, 210, 330}, {360, 250, 390}},         // DRY
            {{340, 310, 330}, {410, 400, 390}},         // GOOD
            {{250, 250, 330}, {290, 300, 380}},         // GOOD-MEDIUM
            {{280, 290, 330}, {340, 340, 400}},         // MEDIUM
            {{490, 470, 420}, {780, 770, 660}},         // MEDIUM-POOR
            {{490, 490, 490}, {580, 580, 580}}};        // POOR

    // Speed per 5 kt
    int[][][] penaltySpeed = {
            {{420, 530, 790}, {520, 610, 930}},   // DRY
            {{560, 600, 790}, {690, 710, 930}},   // GOOD
            {{620, 630, 800}, {740, 740, 920}},   // GOOD-MEDIUM
            {{710, 720, 820}, {840, 840, 950}},   // MEDIUM
            {{850, 850, 900}, {1050, 1060, 1100}},   // MEDIUM-POOR
            {{1040, 1040, 1080}, {1200, 1200, 1230}}};  // POOR

    // Altitude per 1000 ft ABOVE Sea Level
    int[][][] penaltyAltitude = {
            {{150, 180, 290}, {190, 220, 340}},         // DRY
            {{250, 240, 290}, {300, 300, 340}},         // GOOD
            {{220, 220, 300}, {250, 250, 340}},         // GOOD-MEDIUM
            {{250, 240, 300}, {280, 290, 340}},         // MEDIUM
            {{330, 330, 330}, {440, 430, 410}},         // MEDIUM-POOR
            {{410, 410, 420}, {470, 470, 500}}};        // POOR

    // Wind per 5 kt Tailwind
    int[][][] penaltyWind = {
            {{350, 440, 680}, {420, 480, 730}},   // DRY
            {{620, 610, 680}, {730, 710, 730}},   // GOOD
            {{550, 580, 690}, {590, 630, 740}},   // GOOD-MEDIUM
            {{680, 690, 720}, {740, 740, 780}},   // MEDIUM
            {{950, 950, 940}, {1120, 1110, 1080}},   // MEDIUM-POOR
            {{1290, 1290, 1300}, {1370, 1370, 1380}}};  // POOR

    // Temperature per 10° ABOVE ISA
    int[][][] penaltyTemp = {
            {{110, 150, 220}, {140, 170, 260}},         // DRY
            {{190, 190, 220}, {240, 240, 260}},         // GOOD
            {{180, 180, 220}, {210, 210, 260}},         // GOOD-MEDIUM
            {{210, 200, 230}, {240, 250, 260}},         // MEDIUM
            {{280, 280, 280}, {350, 360, 340}},         // MEDIUM-POOR
            {{390, 390, 380}, {440, 440, 430}}};        // POOR

    // Slope per 1% DOWN SLOPE
    int[][][] penaltySlope = {
            {{70, 20, 120}, {80, 20, 140}},   // DRY
            {{160, 120, 120}, {230, 190, 140}},   // GOOD
            {{390, 250, 130}, {300, 300, 150}},   // GOOD-MEDIUM
            {{340, 360, 270}, {430, 440, 340}},   // MEDIUM
            {{510, 520, 560}, {700, 710, 750}},   // MEDIUM-POOR
            {{1070, 1070, 1000}, {1230, 1230, 1150}}};  // POOR

    // One Reverser available
    int[][][] oneReverserAvailable = {
            {{0, 0, 0}, {0, 0, 0}},               // DRY
            {{-30, 0, 0}, {-150, 0, 0}},               // GOOD
            {{-320, 0, 0}, {-450, 0, 0}},               // GOOD-MEDIUM
            {{-320, 0, 0}, {-420, 0, 0}},               // MEDIUM
            {{-430, 0, 0}, {-560, 0, 0}},               // MEDIUM-POOR
            {{0, 0, 0}, {0, 0, 0}}};              // POOR

    // Both Reverser available
    int[][][] twoReverserAvailable = {
            {{0, 0, 0}, {0, 0, 0}}, // DRY
            {{-230, -20, -390}, {-300, -50, -490}}, // GOOD
            {{-540, -410, -390}, {-750, -530, -490}}, // GOOD-MEDIUM
            {{-650, -670, -460}, {-840, -860, -600}}, // MEDIUM
            {{-860, -870, -550}, {-1120, -1140, -770}}, // MEDIUM-POOR
            {{0, 0, 0}, {0, 0, 0}}};// POOR


    // VLS Reference Speed according Landing Weight with Flaps 30/40 and Speed Increment for 15/20
    int[][] speedVLS = {
            // {Weight, VLS 30/40, to add for 15/20}
            {200, 111, 9},
            {205, 111, 11},
            {210, 112, 11},
            {220, 115, 11},
            {235, 119, 11},
            {250, 122, 11},
            {265, 126, 11},
            {280, 129, 11},
            {295, 132, 11},
            {310, 135, 11},
            {325, 139, 11},
            {340, 142, 11},
            {355, 145, 11},
            {370, 148, 11},
            {386, 151, 11}};

    // AprroachClimb-Weights
    int[] apprClimbWeights = {358, 338, 318, 298, 278, 258, 238, 218, 198};

    // Approach Climb Limit for Flap-Setting 15/20
    double[][][] apprClimbLimit1520 = {
            // Weight, ElevationIndex, OAT,
            //      Elevation 0 ft     ,    Elevation 1000 ft      ,       Elevation 2000 ft
            //  0°, 10°, 20°, 30°, 40° ,    0°, 10°, 20°, 30°, 40° ,    0°, 10°, 20°, 30°, 40°
            {{ 2.6, 2.6, 2.6, 2.6, 1.6}, { 2.3, 2.3, 2.3, 2.1, 1.4}, { 2.0, 2.0, 2.0, 1.6, 0.7}},   // Weight 358
            {{ 3.4, 3.4, 3.4, 3.4, 2.3}, { 3.0, 3.0, 3.0, 2.8, 1.8}, { 2.7, 2.7, 2.7, 2.3, 1.4}},   // Weight 338
            {{ 4.2, 4.2, 4.2, 4.2, 3.0}, { 3.9, 3.9, 3.9, 3.7, 2.6}, { 3.6, 3.6, 3.6, 3.1, 2.1}},   // Weight 318
            {{ 5.2, 5.2, 5.2, 5.2, 4.0}, { 4.8, 4.8, 4.8, 4.6, 3.4}, { 4.5, 4.5, 4.5, 4.0, 2.9}},   // Weight 298
            {{ 6.4, 6.4, 6.4, 6.4, 5.0}, { 6.0, 6.0, 6.0, 5.7, 4.4}, { 5.6, 5.6, 5.6, 5.1, 3.9}},   // Weight 278
            {{ 7.6, 7.6, 7.6, 7.6, 6.2}, { 7.2, 7.2, 7.2, 7.0, 5.6}, { 6.9, 6.9, 6.9, 6.3, 5.0}},   // Weight 258
            {{ 8.9, 8.9, 8.9, 8.9, 7.6}, { 8.5, 8.5, 8.5, 8.3, 7.0}, { 8.2, 8.2, 8.2, 7.7, 6.4}},   // Weight 238
            {{10.1,10.1,10.1,10.1, 9.0}, { 9.8,10.1,10.1, 9.6, 8.4}, { 9.5, 9.5, 9.5, 9.0, 7.8}},   // Weight 218
            {{11.4,11.4,11.4,11.4,10.3}, {11.4,11.4,11.4,10.9, 9.8}, {10.8,10.8,10.8,10.4, 9.3}}};  // Weight 198

    // Approach Climb Limit for Flap-Setting 15/15
    double[][][] apprClimbLimit1515 = {
            // Weight, ElevationIndex, OAT,
            //      Elevation 0 ft     ,    Elevation 1000 ft      ,       Elevation 2000 ft
            //  0°, 10°, 20°, 30°, 40° ,    0°, 10°, 20°, 30°, 40° ,    0°, 10°, 20°, 30°, 40°
            {{ 3.6, 3.6, 3.6, 3.6, 2.5}, { 3.3, 3.3, 3.2, 3.0, 2.1}, { 3.0, 2.9, 2.9, 2.6, 1.7}},   // Weight 358
            {{ 4.4, 4.4, 4.4, 4.4, 3.2}, { 4.0, 4.0, 4.0, 3.8, 2.8}, { 3.7, 3.7, 3.7, 3.3, 2.3}},   // Weight 358
            {{ 5.2, 5.2, 5.2, 5.2, 4.0}, { 4.9, 4.9, 4.9, 4.6, 3.5}, { 4.6, 4.5, 4.5, 4.1, 3.0}},   // Weight 358
            {{ 6.2, 6.2, 6.2, 6.2, 4.9}, { 5.9, 5.9, 5.9, 5.6, 4.4}, { 5.5, 5.5, 5.5, 5.0, 3.9}},   // Weight 358
            {{ 7.4, 7.4, 7.4, 7.4, 6.0}, { 7.0, 7.0, 7.0, 6.7, 5.5}, { 6.6, 6.6, 6.6, 6.1, 4.9}},   // Weight 358
            {{ 8.5, 8.5, 8.5, 8.5, 7.3}, { 8.1, 8.1, 8.1, 7.9, 6.6}, { 7.8, 7.8, 7.8, 7.3, 6.0}},   // Weight 358
            {{ 9.6, 9.6, 9.6, 9.6, 8.4}, { 9.3, 9.3, 9.3, 9.0, 7.9}, { 8.9, 8.9, 8.9, 8.5, 7.3}},   // Weight 358
            {{10.7,10.7,10.7,10.7, 9.6}, {10.4,10.4,10.4,10.2, 9.1}, {10.1,10.1,10.1, 9.3, 8.6}},   // Weight 358
            {{11.8,11.8,11.8,11.8,10.8}, {11.5,11.5,11.5,11.4,10.4}, {11.3,11.3,11.3,10.9, 9.9}}};  // Weight 358


    // Autoland per Runway State
    int[] penaltyAutoLand = {820, 940, 840, 970, 1060, 1220};

    // IndexBrakes => Braking-Mode
    private static final int BRAKING_MAX_MANUAL = 0;
    private static final int BRAKING_MEDIUM = 1;
    private static final int BRAKING_LOW = 2;

    // IndexFlpas => Flap Setting
    private static final int FLAPCONFIG_30_40 = 0;
    private static final int FLAPCONFIG_15_20 = 1;

    // IndexRwy => Runway Condition / Braking Action
    private static final int RWY_STATE_DRY = 0;
    private static final int RWY_STATE_GOOD = 1;
    private static final int RWY_STATE_GOODMEDIUM = 2;
    private static final int RWY_STATE_MEDIUM = 3;
    private static final int RWY_STATE_MEDIUMPOOR = 4;
    private static final int RWY_STATE_POOR = 5;

    // IndexWind => Tailwind Component
    private static final int TAILWIND_0 = 0;
    private static final int TAILWIND_5 = 1;
    private static final int TAILWIND_10 = 2;
    private static final int TAILWIND_15 = 3;

    // IndexSpeed => Speed Increment per 5 kts
    private static final int SPEED_0 = 0;
    private static final int SPEED_5 = 1;
    private static final int SPEED_10 = 2;
    private static final int SPEED_15 = 3;
    private static final int SPEED_20 = 4;

    // IndexElevation => Airport elevation
    private static final int ELEVATION_0 = 0;
    private static final int ELEVATION_1000 = 1;
    private static final int ELEVATION_2000 = 2;
    private static final int ELEVATION_3000 = 3;
    private static final int ELEVATION_4000 = 4;
    private static final int ELEVATION_5000 = 5;

    // IndexSlope => Runway Slope
    private static final int SLOPE_0 = 0;
    private static final int SLOPE_1 = 1;
    private static final int SLOPE_2 = 2;
    private static final int SLOPE_3 = 3;

    // IndexReverser => Reverser Available
    private static final int NO_REVERSER = 0;
    private static final int REVERSER_1 = 1;
    private static final int REVERSER_2 = 2;

    // IndexFactor => Multiply Ldg Dist.
    private static final int FACTOR_1_0 = 0;
    private static final int FACTOR_1_1 = 1;
    private static final int FACTOR_1_2 = 2;
    private static final int FACTOR_1_3 = 3;
    private static final int FACTOR_1_4 = 4;
    private static final int FACTOR_1_5 = 5;
    private static final int FACTOR_1_8 = 6;
    private static final int FACTOR_2_5 = 7;


//    private static final double ROUNDING = 0.5;

    int tailWindIndex, flapConfigIndex, temperature, rwyStateIndex, landingWeight,
            weightIndex, speedIndex, elevationIndex, slopeIndex, reverserIndex;

    double factor;

    CheckBox autoLand;
    TextView tvWeight, tvTemperature, tvBA_MaxManual, tvBA_Medium, tvBA_Low, tvVLS, tvVapp,
            tvAppClimbLimit;
    Spinner spRwyState, spFlapSetting, spSpeedIncrement, spElevation, spTailwind, spSlope,
            spReverser, spMultiplikator;
    Button btReset;
    Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_input);
        setTitle(R.string.title);

        initViews();
        setupAdapters();
        setupListeners();
        setInitialValues();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.menu = menu;
        // initialize righthand side menu
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch (id)
        {
            case R.id.about:
                Toast.makeText(this, "Version: ".concat(BuildConfig.VERSION_NAME), Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews()
    {
        autoLand = findViewById(R.id.auto_land);

        // Spinner for Runway Condition
        spRwyState = findViewById(R.id.rwy_state);

        // Spinner for Flap-Configuration
        spFlapSetting = findViewById(R.id.flap_setting);

        // Landing Weight
        tvWeight = this.findViewById(R.id.landing_weight);

        //Spinner for Speed-Increment
        spSpeedIncrement = findViewById(R.id.speed_increment);

        // Spinner for Airport elevation
        spElevation = findViewById(R.id.airport_elevation);

        // Spinner for Tailwind-Compnoent
        spTailwind = findViewById(R.id.tailwind);

        // Temperature
        tvTemperature = this.findViewById(R.id.temperature);

        // Spinner for Runway Slope
        spSlope = findViewById(R.id.rwy_slope);

        // Spinner for Reverser Available
        spReverser = findViewById(R.id.reverser_usable);

        // Spinner for Multiplikator
        spMultiplikator = findViewById(R.id.multiplikator_spinner);

        // TextViews for Braking Actions
        tvBA_MaxManual = findViewById(R.id.summary_manual);
        tvBA_Medium = findViewById(R.id.summary_medium);
        tvBA_Low = findViewById(R.id.summary_low);

        // Button Reset
        btReset = findViewById(R.id.button_reset);

        // TextView for Speed VLS
        tvVLS = findViewById(R.id.speed_vls);

        // TextView for Speed Vapp
        tvVapp = findViewById(R.id.speed_app);

        // TextView for AppClimbLimit
        tvAppClimbLimit = findViewById(R.id.app_climb_limit);
    }

    private void setupAdapters()
    {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.brakingaction,
                R.layout.my_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spRwyState.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.flapconfig,
                R.layout.my_spinner_item);
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spFlapSetting.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.speed,
                R.layout.my_spinner_item);
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spSpeedIncrement.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.elevation,
                R.layout.my_spinner_item);
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spElevation.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.tailwind,
                R.layout.my_spinner_item);
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spTailwind.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.slope,
                R.layout.my_spinner_item);
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spSlope.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.reverser,
                R.layout.my_spinner_item);
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spReverser.setAdapter(adapter);

        adapter = ArrayAdapter.createFromResource(this, R.array.factor,
                R.layout.my_spinner_item);
        adapter.setDropDownViewResource(R.layout.my_spinner_dropdown_item);
        spMultiplikator.setAdapter(adapter);
    }

    private void setupListeners()
    {
        spRwyState.setOnItemSelectedListener(this);
        spFlapSetting.setOnItemSelectedListener(this);
        tvWeight.setOnClickListener(this);
        spSpeedIncrement.setOnItemSelectedListener(this);
        spElevation.setOnItemSelectedListener(this);
        spTailwind.setOnItemSelectedListener(this);
        tvTemperature.setOnClickListener(this);
        spSlope.setOnItemSelectedListener(this);
        spReverser.setOnItemSelectedListener(this);
        spMultiplikator.setOnItemSelectedListener(this);
        btReset.setOnClickListener(this);
    }

    /**
     * Initialize variables and
     * show initial values on screen
     */
    private void setInitialValues()
    {
        spRwyState.setSelection(RWY_STATE_DRY);
        spFlapSetting.setSelection(FLAPCONFIG_30_40);

        landingWeight = 300;
        weightIndex = 0;
        tvWeight.setText(String.valueOf(landingWeight)
                .concat(getString(R.string.blank))
                .concat(getString(R.string.unit_lbs)));

        spSpeedIncrement.setSelection(SPEED_5);
        spElevation.setSelection(ELEVATION_0);
        spTailwind.setSelection(TAILWIND_0);

        temperature = isaTemp();
        tvTemperature.setText(getText(R.string.isa));

        spSlope.setSelection(SLOPE_0);
        spReverser.setSelection(REVERSER_2);
        spMultiplikator.setSelection(FACTOR_1_0);

        if (autoLand.isChecked()) autoLand.toggle();

        summary();
    }

    private void summary()
    {
        tvBA_MaxManual.setText(String.valueOf(calcDistance(BRAKING_MAX_MANUAL))
                .concat(getResources().getString(R.string.unit_feet)));

        tvBA_Medium.setText(String.valueOf(calcDistance(BRAKING_MEDIUM))
                .concat(getResources().getString(R.string.unit_feet)));

        tvBA_Low.setText(String.valueOf(calcDistance(BRAKING_LOW))
                .concat(getResources().getString(R.string.unit_feet)));

        int vls = calcVLS(landingWeight, flapConfigIndex);
        tvVLS.setText(String.valueOf(vls));
        tvVapp.setText(String.valueOf(vls + speedIndex * 5));
        tvAppClimbLimit.setText(calcAppClimbLimit(landingWeight, elevationIndex, temperature, flapConfigIndex));
    }

    private int calcDistance(int brakingMode)
    {
        double landingDistance = ((referenceDistance[rwyStateIndex][flapConfigIndex][brakingMode]
                + calcWeight(brakingMode)
                + calcSpeed(brakingMode)
                + calcAltitude(brakingMode)
                + calcWind(brakingMode)
                + calcTemp(brakingMode)
                + calcSlope(brakingMode)
                + calcReverser(brakingMode)
                + calcAutoLand(rwyStateIndex))
                * factor);
        return (int) landingDistance;

    }

    /*
    What happens if user clicks a button
     */
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.landing_weight:
                showDialogWeight();
                break;

            case R.id.temperature:
                showDialogTemperature();
                break;

            case R.id.button_reset:
                setInitialValues();
                Toast.makeText(this, getString(R.string.initial_values_set), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void showDialogWeight()
    {
        final int minWeight = 200;
        final int maxWeight = 385;
        final int stepSize = 5;
        int maxValue = (maxWeight - minWeight) / stepSize + 1;

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog);
        dialog.setTitle("Landing Weight");
        Button btnSet = dialog.findViewById(R.id.button1);
        Button btnCancel = dialog.findViewById(R.id.button2);
        TextView tv = dialog.findViewById(R.id.unit_title);
        tv.setText(R.string.unit_lbs);
        final NumberPicker np = dialog.findViewById(R.id.numberPicker1);
        np.setMinValue(0);
        np.setMaxValue(maxValue - 1);
        np.setValue((landingWeight - minWeight) / stepSize);
        np.setWrapSelectorWheel(false);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        String[] values = new String[maxValue];
        int key = 0;

        for (int i = minWeight; i <= maxWeight; i = i + stepSize)
        {
            values[key] = i + ".0 ";
            key++;
        }

        np.setDisplayedValues(values);

        btnSet.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                landingWeight = np.getValue() * stepSize + minWeight;
                tvWeight.setText(String.valueOf(landingWeight)
                        .concat(".0 ")
                        .concat(getResources().getString(R.string.unit_lbs)));
                tvWeight.setTextColor(Color.parseColor("#000000"));
                dialog.dismiss();
                summary();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Eingabe der Tailwindkomponente
     */
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.rwy_state)
        {
            switch (position)
            {
                case 0:
                    rwyStateIndex = RWY_STATE_DRY;
                    summary();
                    break;
                case 1:
                    rwyStateIndex = RWY_STATE_GOOD;
                    summary();
                    break;
                case 2:
                    rwyStateIndex = RWY_STATE_GOODMEDIUM;
                    summary();
                    break;
                case 3:
                    rwyStateIndex = RWY_STATE_MEDIUM;
                    summary();
                    break;
                case 4:
                    rwyStateIndex = RWY_STATE_MEDIUMPOOR;
                    summary();
                    break;
                case 5:
                    rwyStateIndex = RWY_STATE_POOR;
                    summary();
                    break;
            }
        }
        else if (spinner.getId() == R.id.flap_setting)
        {
            switch (position)
            {
                case 0:
                    flapConfigIndex = FLAPCONFIG_30_40;
                    summary();
                    break;
                case 1:
                    flapConfigIndex = FLAPCONFIG_15_20;
                    checkAutoland();
                    break;
            }
        }
        else if (spinner.getId() == R.id.speed_increment)
        {
            switch (position)
            {
                case 0:
                    speedIndex = SPEED_0;
                    summary();
                    break;
                case 1:
                    speedIndex = SPEED_5;
                    summary();
                    break;
                case 2:
                    speedIndex = SPEED_10;
                    summary();
                    break;
                case 3:
                    speedIndex = SPEED_15;
                    summary();
                    break;
                case 4:
                    speedIndex = SPEED_20;
                    summary();
                    break;
            }
        }
        else if (spinner.getId() == R.id.airport_elevation)
        {
            switch (position)
            {
                case 0:
                    elevationIndex = ELEVATION_0;
                    break;
                case 1:
                    elevationIndex = ELEVATION_1000;
                    break;
                case 2:
                    elevationIndex = ELEVATION_2000;
                    break;
                case 3:
                    elevationIndex = ELEVATION_3000;
                    break;
                case 4:
                    elevationIndex = ELEVATION_4000;
                    break;
                case 5:
                    elevationIndex = ELEVATION_5000;
                    break;
            }
            if (tvTemperature.getText().toString().equals(getString(R.string.isa)))
            {
                temperature = isaTemp();
            }
            summary();

        }
        else if (spinner.getId() == R.id.tailwind)
        {
            switch (position)
            {
                case 0:
                    tailWindIndex = TAILWIND_0;
                    summary();
                    break;
                case 1:
                    tailWindIndex = TAILWIND_5;
                    summary();
                    break;
                case 2:
                    tailWindIndex = TAILWIND_10;
                    summary();
                    break;
                case 3:
                    tailWindIndex = TAILWIND_15;
                    summary();
                    break;
            }
        }
        else if (spinner.getId() == R.id.rwy_slope)
        {
            switch (position)
            {
                case 0:
                    slopeIndex = SLOPE_0;
                    summary();
                    break;
                case 1:
                    slopeIndex = SLOPE_1;
                    summary();
                    break;
                case 2:
                    slopeIndex = SLOPE_2;
                    summary();
                    break;
                case 3:
                    slopeIndex = SLOPE_3;
                    summary();
                    break;
            }
        }
        else if (spinner.getId() == R.id.reverser_usable)
        {
            switch (position)
            {
                case 0:
                    reverserIndex = NO_REVERSER;
                    summary();
                    break;
                case 1:
                    reverserIndex = REVERSER_1;
                    summary();
                    break;
                case 2:
                    reverserIndex = REVERSER_2;
                    summary();
                    break;
            }
        }
        else if (spinner.getId() == R.id.multiplikator_spinner)
        {
            switch (position)
            {
                case FACTOR_1_0:
                    factor = 1.0;
                    summary();
                    break;
                case FACTOR_1_1:
                    factor = 1.1;
                    summary();
                    break;
                case FACTOR_1_2:
                    factor = 1.2;
                    summary();
                    break;
                case FACTOR_1_3:
                    factor = 1.3;
                    summary();
                    break;
                case FACTOR_1_4:
                    factor = 1.4;
                    summary();
                    break;
                case FACTOR_1_5:
                    factor = 1.5;
                    summary();
                    break;
                case FACTOR_1_8:
                    factor = 1.8;
                    summary();
                    break;
                case FACTOR_2_5:
                    factor = 2.5;
                    summary();
                    break;
            }
        }
    }

    public void onNothingSelected(AdapterView<?> parent)
    {
    }

    public void showDialogTemperature()
    {
        final int maxValue = 60;
        final int minValue = -30;
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog);
        dialog.setTitle(getString(R.string.temperature));
        Button btnSet = dialog.findViewById(R.id.button1);
        Button btnISA = dialog.findViewById(R.id.button2);
        btnISA.setText(R.string.isa);
        TextView tv = dialog.findViewById(R.id.unit_title);
        tv.setText(R.string.unit_temp);
        final NumberPicker np = dialog.findViewById(R.id.numberPicker1);
        np.setDisplayedValues(null);
        np.setMinValue(0);
        np.setMaxValue(maxValue - minValue);
        if (tvTemperature.getText().toString().equals(getString(R.string.isa)))
        {
            temperature = isaTemp();
        }
        np.setValue(temperature - maxValue - 1);
        np.setWrapSelectorWheel(false);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        String[] values = new String[maxValue - minValue + 1];
        int key = 0;

        for (int i = minValue; i <= maxValue; i++)
        {
            values[key] = String.valueOf(i);
            key++;
        }

        np.setDisplayedValues(values);

        btnSet.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                temperature = np.getValue() + minValue;
                tvTemperature.setText(String.valueOf(temperature)
                        .concat(getString(R.string.unit_temp)));
//                tvTemperature.setTextColor(Color.parseColor("#000000"));
                dialog.dismiss();
                summary();
            }
        });
        btnISA.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                temperature = isaTemp();
                tvTemperature.setText(getString(R.string.isa));
                summary();
            }
        });
        dialog.show();
    }

    private int calcWeight(int brakingMode)
    {
        int weightIndex;
        if (landingWeight < REFERENCE_WEIGHT)
        {
            weightIndex = (REFERENCE_WEIGHT - landingWeight) / 10;
            return penaltyWeightBelow[rwyStateIndex][flapConfigIndex][brakingMode] * weightIndex;
        }
        else if (landingWeight > REFERENCE_WEIGHT)
        {
            weightIndex = (int) ((double)(landingWeight - REFERENCE_WEIGHT) / 10 + 0.9);
            return penaltyWeightAbove[rwyStateIndex][flapConfigIndex][brakingMode] * weightIndex;
        }
        return 0;
    }

    private int calcSpeed(int brakingMode)
    {
        return penaltySpeed[rwyStateIndex][flapConfigIndex][brakingMode] * speedIndex;
    }

    private int calcAltitude(int brakingMode)
    {
        return penaltyAltitude[rwyStateIndex][flapConfigIndex][brakingMode] * elevationIndex;
    }

    private int calcWind(int brakingMode)
    {
        return tailWindIndex * penaltyWind[rwyStateIndex][flapConfigIndex][brakingMode];
    }

    private int calcTemp(int brakingMode)
    {
        if (temperature > isaTemp())
        {
            // tempIndex => per 10°C above ISA rounded up to the next 10°C
            int tempIndex = (int) ((temperature - isaTemp() + 9) * 0.1);
            return penaltyTemp[rwyStateIndex][flapConfigIndex][brakingMode] * (tempIndex);
        }
        else return 0;
    }

    private int isaTemp()
    {
        return (15 - (elevationIndex * 2));
    }

    private int calcSlope(int brakingMode)
    {
        return penaltySlope[rwyStateIndex][flapConfigIndex][brakingMode] * slopeIndex;
    }

    private int calcReverser(int brakingMode)
    {
        if (reverserIndex == REVERSER_1)
            return oneReverserAvailable[rwyStateIndex][flapConfigIndex][brakingMode];
        else if (reverserIndex == REVERSER_2)
            return twoReverserAvailable[rwyStateIndex][flapConfigIndex][brakingMode];
        return 0;
    }

    private int calcAutoLand(int rwyState)
    {
        if (autoLand.isChecked()) return penaltyAutoLand[rwyState];
        else return 0;
    }

    public void setAutoLand(View view)
    {
        checkAutoland();
    }

    private void checkAutoland()
    {
        if (autoLand.isChecked() && flapConfigIndex == FLAPCONFIG_15_20)
        {
            autoLand.toggle();
            Toast.makeText(this, "No Autoland with Flaps 15/20", Toast.LENGTH_SHORT).show();
        }
        summary();
    }

    private int calcVLS(int landingWeight, int flapIndex)
    {
        final int WEIGHT = 0;
        final int VLS_30_40 = 1;
        final int ADDITION_FOR_15_20 = 2;

        for (int i = 0; i < speedVLS.length; i++)
        {
            // Get speeds of weight rounded up to next 10
            if (landingWeight >= speedVLS[i][WEIGHT] && landingWeight <= speedVLS[i + 1][WEIGHT])
            {
                int speed = speedVLS[i + 1][VLS_30_40];

                if (flapIndex == FLAPCONFIG_15_20)
                {
                    if (landingWeight == speedVLS[0][WEIGHT])
                    {
                        speed =  speed + speedVLS[0][ADDITION_FOR_15_20];
                    }
                    else
                    {
                        speed = speed + speedVLS[i + 1][ADDITION_FOR_15_20];
                    }
                }
                return speed;
            }
        }
        return -1;
    }

    private String calcAppClimbLimit(int weight, int elevation, int temperature, int flapConfig)
    {
        int weightIndex;
        int oatIndex;
        double appClimbLimit;

        // Check if values are inside scope of tables
        if (weight > 358 || temperature > 40 || elevation > 2)
        {
            return "???";
        }

        // get index for actual landing weight
        for (weightIndex = 0; weightIndex <= apprClimbWeights.length; weightIndex++)
        {
            if (weight > apprClimbWeights[weightIndex])
            {
                weightIndex--;
                break;
            }
        }

        // get index for actual temperature of aerodrome
        for (oatIndex = 0; oatIndex <= 4; oatIndex++)
        {
            if (temperature <= oatIndex * 10)
            {
                break;
            }
        }

        //  PF -> "Go Around, Flaps"
        // if approach config is 30/40 => G/A wil be 15/20
        if (flapConfig == FLAPCONFIG_30_40)
        {
            appClimbLimit = apprClimbLimit1520[weightIndex][elevation][oatIndex];
        }
        else
        // if approach config is 15/20 => G/A wil be 15/15
        {
            appClimbLimit = apprClimbLimit1515[weightIndex][elevation][oatIndex];
        }
        return String.valueOf(appClimbLimit);
    }
}
